# KomiDL - A gallery downloader
# Copyright (C) 2020 DNSheng
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""This module registers custom extractors and instantiates them for KomiDL"""

from .extractor import REGISTERED
from .ehentai import EHentaiEX
from .hentai2read import Hentai2ReadEX
from .imgur import ImgurEX
from .mangadex import MangaDexEX
from .mangahub import MangaHubEX
from .mangasushi import MangaSushiEX
from .nhentai import NHentaiEX
from .tsumino import TsuminoEX


def get_extractors():
    """Return a list of instantiated registered extractors"""
    return [klass() for klass in REGISTERED.values()]
