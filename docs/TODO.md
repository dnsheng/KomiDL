### TODO:

```
+ Mangahub fix Javascript to AST
+ Mangadex HTTP 409
	+ Some groups have a delay policy when posting to the site
	+ (Ex. A link is available but images not shown for a certain delay)
	+ Should catch 409 errors in _retrieve_chapter_data() and skip past those
+ Mangadex numbering issue
	+ For a gallery with only a single "Chapter 0", title numbering shows "[0001-0000]"
+ Adding more options
	+ Limit chapters downloaded (specify start and end chapter, inclusive)
	+ Suppress warnings to output
+ Warning when downloading that certain options not applicable (eg. language)
	+ Only show when option not default
+ Possible features:
	+ Update existing gallery with new chapters
+ Unit tests
+ Support for more websites
```
