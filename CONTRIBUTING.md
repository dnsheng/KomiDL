# Contributing

Thank you for being interested in contributing to KomiDL!

## Reporting bugs

#### Check if the bug has already been reported

If so, you can comment on the existing bug report with your details.
This ensures that the issue tracker stays organized.

If the bug has not been reported before, you can proceed with creating a
new issue in the tracker.

#### Writing a bug report

A well-written bug report should contain the following:

    + A descriptive title
    + Operating system and version
    + Python version
    + Dependency versions
    + Program version (commit id if possible)
    + How the program was called
    + Output from the program
    + Expected behaviour

## Writing code

Code written should follow PEP 8 (style guide) and
PEP 257 (docstring conventions).

Furthermore, the code should pass all existing tests.

If you added new functionality, you may need to create new tests.

If you fixed a bug, a new test case should be created to prevent that bug
from reoccurring in the future.

